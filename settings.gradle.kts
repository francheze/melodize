rootProject.buildFileName = "build.gradle.kts"

include(":app")
include(":domain")
include(":data")
