object Dependencies {

    object Android {
        const val minSdkVersion = 21
        const val targetSdkVersion = 31
        const val compileSdkVersion = 31
        const val gradleBuildTools = "com.android.tools.build:gradle:7.0.2"
        const val androidxAppCompat = "androidx.appcompat:appcompat:1.3.1"
        const val androidxFragment = "androidx.fragment:fragment-ktx:1.3.6"
        const val androidxCore = "androidx.core:core-ktx:1.6.0"
        const val sharedPreferences = "androidx.preference:preference:1.1.1"
    }

    object Ktx {
        private const val kotlinVersion = "1.5.30"
        const val kotlinPlugin = "org.jetbrains.kotlin:kotlin-gradle-plugin:$kotlinVersion"
        const val kotlinReflect = "org.jetbrains.kotlin:kotlin-reflect:$kotlinVersion"
        const val stdlib = "org.jetbrains.kotlin:kotlin-stdlib-jdk7:$kotlinVersion"
    }

    object Ui{
        const val materialAndroid = "com.google.android.material:material:1.4.0"
        const val constraintLayout = "androidx.constraintlayout:constraintlayout:2.1.0"
        const val recyclerView = "androidx.recyclerview:recyclerview:1.2.0"
        const val swipeRefreshLayout = "androidx.swiperefreshlayout:swiperefreshlayout:1.1.0"
        const val lottie = "com.airbnb.android:lottie:4.1.0"
    }

    object Hilt {
		private const val hiltVersion = "2.38.1"
		const val hiltPlugin = "com.google.dagger:hilt-android-gradle-plugin:$hiltVersion"
		const val hiltAndroid = "com.google.dagger:hilt-android:$hiltVersion"
        const val hiltCompiler = "com.google.dagger:hilt-android-compiler:$hiltVersion"
    }

    object Log {
        const val timber = "com.jakewharton.timber:timber:5.0.1"
    }

    object Lifecycle {
        private const val lifecycleVersion = "2.3.1"
        const val lifecycleLiveData = "androidx.lifecycle:lifecycle-livedata-ktx:$lifecycleVersion"
        const val lifecycleExtensions = "androidx.lifecycle:lifecycle-viewmodel-ktx:$lifecycleVersion"
        const val lifecycleReactiveStreams = "androidx.lifecycle:lifecycle-reactivestreams:$lifecycleVersion"
        const val lifecycleReactiveStreamsExtensions = "androidx.lifecycle:lifecycle-reactivestreams-ktx:$lifecycleVersion"
    }

    object Coroutines {
        private const val coroutinesVersion = "1.5.2"
        const val coroutinesCore = "org.jetbrains.kotlinx:kotlinx-coroutines-core:$coroutinesVersion"
        const val coroutinesAndroid = "org.jetbrains.kotlinx:kotlinx-coroutines-android:$coroutinesVersion"
        const val coroutinesTest = "org.jetbrains.kotlinx:kotlinx-coroutines-test:$coroutinesVersion"
    }

    object Retrofit {
        private const val retrofitVersion = "2.9.0"
        const val retrofit = "com.squareup.retrofit2:retrofit:$retrofitVersion"
        const val retrofitGson = "com.squareup.retrofit2:converter-gson:$retrofitVersion"
        const val retrofitInterceptor = "com.squareup.okhttp3:logging-interceptor:4.5.0"
    }

    object Test {
        const val junit = "junit:junit:4.13.2"
        const val androidJunit = "androidx.test.ext:junit:1.1.3"
        const val mockito = "org.mockito:mockito-core:3.12.4"
        const val mockitoInline = "org.mockito:mockito-inline:2.13.0"
        const val assertj = "org.assertj:assertj-core:3.20.2"
        const val coreTesting = "androidx.arch.core:core-testing:2.1.0"
        const val espresso = "androidx.test.espresso:espresso-core:3.4.0"
    }

    object Room {
        private const val roomVersion = "2.3.0"
        const val roomRuntime = "androidx.room:room-runtime:$roomVersion"
        const val roomCompiler = "androidx.room:room-compiler:$roomVersion"
        const val roomKtx = "androidx.room:room-ktx:$roomVersion"
        const val roomTesting = "androidx.room:room-testing:$roomVersion"
    }

    object Glide {
        private const val glideVersion = "4.12.0"
        const val glide = "com.github.bumptech.glide:glide:$glideVersion"
        const val glideCompiler = "com.github.bumptech.glide:compiler:$glideVersion"
    }
}
