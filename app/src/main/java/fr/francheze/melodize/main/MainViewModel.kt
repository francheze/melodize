package fr.francheze.melodize.main

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import fr.francheze.domain.songs.UpdateSongsUseCase
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(private val updateSongsUseCase: UpdateSongsUseCase) : ViewModel() {
    fun refreshSongs() {
        viewModelScope.launch {
            kotlin.runCatching {
                updateSongsUseCase.refreshSongs()
            }.onFailure {
                Timber.e(it)
            }
        }
    }
}