package fr.francheze.melodize.main

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import fr.francheze.melodize.R
import fr.francheze.melodize.databinding.ActivityMainBinding
import fr.francheze.melodize.favorites.FavoritesFragment
import fr.francheze.melodize.home.HomeFragment
import fr.francheze.melodize.player.PlayerFragment
import fr.francheze.melodize.search.SearchFragment
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    companion object {
        const val SELECTED_ITEM_KEY = "SELECTED_ITEM"
    }

    private val mainViewModel: MainViewModel by viewModels()

    @Inject lateinit var homeFragment: HomeFragment
    @Inject lateinit var searchFragment: SearchFragment
    @Inject lateinit var favoritesFragment: FavoritesFragment
    @Inject lateinit var playerFragment: PlayerFragment

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.bottomNavigationView.setOnItemSelectedListener {
            when (it.itemId) {
                R.id.home -> setCurrentFragment(homeFragment)
                R.id.search -> setCurrentFragment(searchFragment)
                R.id.favorites -> setCurrentFragment(favoritesFragment)
                R.id.player -> setCurrentFragment(playerFragment)
            }
            true
        }

        mainViewModel.refreshSongs()

        if (savedInstanceState == null) {
            binding.bottomNavigationView.selectedItemId = R.id.home
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putInt(SELECTED_ITEM_KEY, binding.bottomNavigationView.selectedItemId)
        super.onSaveInstanceState(outState)
    }

    fun goToPlayerFragment() {
        binding.bottomNavigationView.selectedItemId = R.id.player
    }

    private fun setCurrentFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.fragmentFrameLayout, fragment)
            commit()
        }
    }
}