package fr.francheze.melodize.common

import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import fr.francheze.domain.songs.Song
import fr.francheze.melodize.R
import fr.francheze.melodize.common.utils.ImageProcessingUtils.addRandomColorBackground
import fr.francheze.melodize.common.utils.ImageProcessingUtils.loadImage
import fr.francheze.melodize.databinding.ItemSongRecyclerBinding

class SongRecyclerAdapter(
    val songs: MutableList<Song>,
    private val songSelectedListener: SongSelectedListener,
    private val favoriteListener: FavoriteListener
) : RecyclerView.Adapter<SongRecyclerAdapter.ViewHolder>() {

    class ViewHolder(private val binding: ItemSongRecyclerBinding) : RecyclerView.ViewHolder(binding.root), RequestListener<Drawable> {
        fun bind(song: Song, clickListener: View.OnClickListener, favoriteListener: View.OnClickListener) {
            binding.songTitle.text = song.title
            binding.root.setOnClickListener(clickListener)
            toggleFavoriteState(song.isFavorite)
            binding.favoriteToggle.setOnClickListener {
                toggleFavoriteState(!song.isFavorite)
                favoriteListener.onClick(it)
            }
            binding.albumCover.loadImage(song.thumbnailUrl, this)
        }

        private fun toggleFavoriteState(isFavorite: Boolean) {
            binding.favoriteToggle.setImageResource(
                if (isFavorite) {
                    R.drawable.ic_favorite
                } else {
                    R.drawable.ic_not_favorite
                }
            )
        }

        override fun onLoadFailed(
            e: GlideException?,
            model: Any?,
            target: Target<Drawable>?,
            isFirstResource: Boolean
        ): Boolean {
            e?.logRootCauses(SongRecyclerAdapter::class.simpleName)
            binding.albumCover.addRandomColorBackground()
            binding.albumCover.setImageResource(R.drawable.ic_album)
            return false
        }

        override fun onResourceReady(
            resource: Drawable?,
            model: Any?,
            target: Target<Drawable>?,
            dataSource: DataSource?,
            isFirstResource: Boolean
        ) = false
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemSongRecyclerBinding.inflate(LayoutInflater.from(viewGroup.context), viewGroup, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        val song = songs[position]
        viewHolder.bind(
            song,
            { songSelectedListener.onSongSelected(song) },
            {
                val newState = !song.isFavorite
                songs[songs.indexOf(song)] = song.copy(isFavorite = newState)
                favoriteListener.addAsFavorite(song, newState)
            }
        )
    }

    override fun getItemCount() = songs.size

}

interface SongSelectedListener {
    fun onSongSelected(song: Song)
}

interface FavoriteListener {
    fun addAsFavorite(song: Song, isFavorite: Boolean)
}