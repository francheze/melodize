package fr.francheze.melodize.common.network

import android.content.Context
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities.*
import android.net.NetworkRequest
import android.os.Build
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import fr.francheze.melodize.common.network.NetworkState.*
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject

class NetworkStateChecker @Inject constructor(@ApplicationContext val appContext: Context) : LifecycleObserver {

    private lateinit var networkChangeListener: NetworkChangeListener

    private val networkRequest = getNetworkRequest()
    private val networkCallback = getNetworkCallBack()

    fun observe(lifecycle: Lifecycle, networkChangeListener: NetworkChangeListener) {
        this.networkChangeListener = networkChangeListener
        lifecycle.addObserver(this)
    }

    fun isNetworkAvailable() =
        (appContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?)?.hasNetwork() ?: true

    private fun getNetworkCallBack(): ConnectivityManager.NetworkCallback {
        return object : ConnectivityManager.NetworkCallback() {
            override fun onAvailable(network: Network) {
                super.onAvailable(network)
                networkChangeListener.onStateChanged(Available)
            }

            override fun onLost(network: Network) {
                super.onLost(network)
                networkChangeListener.onStateChanged(NotAvailable)
            }
        }
    }

    private fun getConnectivityManager() = appContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

    private fun getNetworkRequest(): NetworkRequest {
        return NetworkRequest.Builder()
                .addTransportType(TRANSPORT_WIFI)
                .addTransportType(TRANSPORT_CELLULAR)
                .addTransportType(TRANSPORT_ETHERNET)
                .build()
    }

    private fun ConnectivityManager.hasNetwork() =
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getNetworkCapabilities(activeNetwork)?.let { networkCapabilities ->
                when {
                    networkCapabilities.hasTransport(TRANSPORT_CELLULAR) -> true
                    networkCapabilities.hasTransport(TRANSPORT_WIFI) -> true
                    networkCapabilities.hasTransport(TRANSPORT_ETHERNET) -> true
                    else -> false
                }
            } ?: false
        } else {
            activeNetworkInfo?.isConnected ?: false
        }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    private fun register() {
        getConnectivityManager().registerNetworkCallback(networkRequest, networkCallback)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    private fun unregister() {
        getConnectivityManager().unregisterNetworkCallback(networkCallback)
    }
}

