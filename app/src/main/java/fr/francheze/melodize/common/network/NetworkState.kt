package fr.francheze.melodize.common.network

sealed class NetworkState {
    object Available : NetworkState()
    object NotAvailable : NetworkState()
    object Unknown : NetworkState()
}
