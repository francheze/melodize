package fr.francheze.melodize.common

import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import fr.francheze.domain.favorites.Favorite
import fr.francheze.melodize.R
import fr.francheze.melodize.common.utils.ImageProcessingUtils.addRandomColorBackground
import fr.francheze.melodize.common.utils.ImageProcessingUtils.loadImage
import fr.francheze.melodize.databinding.ItemFavoriteRecyclerBinding

class FavoriteRecyclerAdapter(
    val favoriteList: MutableList<Favorite>,
    private val favoriteListener: FavoriteSelectedListener,
    private val multiSelectionModeListener: MultiSelectionModeListener
) : RecyclerView.Adapter<FavoriteRecyclerAdapter.ViewHolder>() {
    val selectedFavorites = hashMapOf<Int, Favorite>()
    var isInMultiSelectionMode = false

    class ViewHolder(private val binding: ItemFavoriteRecyclerBinding) : RecyclerView.ViewHolder(binding.root), RequestListener<Drawable> {
        fun bind(
            favorite: Favorite,
            shortClickListener: View.OnClickListener,
            longClickListener: View.OnLongClickListener,
            isSelected: Boolean
        ) {
            binding.songTitle.text = favorite.title
            binding.root.setOnClickListener(shortClickListener)
            binding.root.setOnLongClickListener(longClickListener)
            toggleSelectionState(isSelected)
            binding.albumCover.loadImage(favorite.thumbnailUrl, this)
        }

        fun toggleSelectionState(isSelected: Boolean) {
            binding.songTitle.background = ColorDrawable(ContextCompat.getColor(
                binding.root.context,
                if (isSelected) R.color.colorAccent else R.color.colorCardBackground
            ))
        }

        override fun onLoadFailed(
            e: GlideException?,
            model: Any?,
            target: Target<Drawable>?,
            isFirstResource: Boolean
        ): Boolean {
            e?.logRootCauses(FavoriteRecyclerAdapter::class.simpleName)
            binding.albumCover.addRandomColorBackground()
            binding.albumCover.setImageResource(R.drawable.ic_album)
            return false
        }

        override fun onResourceReady(
            resource: Drawable?,
            model: Any?,
            target: Target<Drawable>?,
            dataSource: DataSource?,
            isFirstResource: Boolean
        ) = false
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemFavoriteRecyclerBinding.inflate(LayoutInflater.from(viewGroup.context), viewGroup, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        val favorite = favoriteList[position]
        viewHolder.bind(
            favorite,
            {
                if (isInMultiSelectionMode) {
                    toggleSelectionForFavorite(favorite, viewHolder)
                } else {
                    favoriteListener.onFavoriteSelected(favorite)
                }
            },
            {
                toggleMultiSelectionMode(true)
                toggleSelectionForFavorite(favorite, viewHolder)
                true
            },
            selectedFavorites.keys.contains(favorite.id)
        )
    }

    private fun toggleSelectionForFavorite(favorite: Favorite, viewHolder: ViewHolder) {
        if (selectedFavorites.keys.contains(favorite.id)) {
            selectedFavorites.remove(favorite.id)
            viewHolder.toggleSelectionState(false)
            if (selectedFavorites.isEmpty()) {
                toggleMultiSelectionMode(false)
            }
        } else {
            selectedFavorites[favorite.id] = favorite
            viewHolder.toggleSelectionState(true)
        }
    }

    fun toggleMultiSelectionMode(isInMultiSelectionMode: Boolean) {
        this.isInMultiSelectionMode = isInMultiSelectionMode
        multiSelectionModeListener.onMultiSelectionModeChanged(isInMultiSelectionMode)
    }

    override fun getItemCount() = favoriteList.size

}

interface FavoriteSelectedListener {
    fun onFavoriteSelected(favorite: Favorite)
}

interface MultiSelectionModeListener {
    fun onMultiSelectionModeChanged(isInMultiSelectionMode: Boolean)
}