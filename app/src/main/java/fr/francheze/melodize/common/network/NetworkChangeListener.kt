package fr.francheze.melodize.common.network

interface NetworkChangeListener {
    fun onStateChanged(networkState: NetworkState)
}
