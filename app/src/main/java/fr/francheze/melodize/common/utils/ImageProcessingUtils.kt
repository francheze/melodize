package fr.francheze.melodize.common.utils

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.model.GlideUrl
import com.bumptech.glide.load.model.LazyHeaders
import com.bumptech.glide.request.RequestListener
import java.util.*

object ImageProcessingUtils {
    fun ImageView.loadImage(url: String?, listener: RequestListener<Drawable>) {
        // Solution suggested at https://stackoverflow.com/questions/66284817/glide-unable-to-load-url-images-of-this-type to prevent error 410
        val USER_AGENT = "Mozilla/5.0 (Linux; Android 11) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.181 Mobile Safari/537.36"
        val glideUrl = GlideUrl(
            url ?: " ",
            LazyHeaders.Builder().addHeader("User-Agent", USER_AGENT).build()
        )
        Glide.with(context)
                .load(glideUrl)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .addListener(listener)
                .into(this)
    }

    fun ImageView.addRandomColorBackground() {
        if (this.background == null) {
            val randomColor = Random().let { Color.argb(255, it.nextInt(256), it.nextInt(256), it.nextInt(256)) }
            this.background = ColorDrawable(randomColor)
        }
    }
}