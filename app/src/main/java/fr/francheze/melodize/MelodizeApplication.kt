package fr.francheze.melodize

import android.app.Application
import dagger.hilt.android.HiltAndroidApp
import timber.log.Timber

@HiltAndroidApp
class MelodizeApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        // init log configuration
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }
}