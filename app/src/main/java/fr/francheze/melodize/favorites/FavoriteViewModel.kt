package fr.francheze.melodize.favorites

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import fr.francheze.domain.favorites.Favorite
import fr.francheze.domain.favorites.UpdateFavoritesUseCase
import fr.francheze.domain.favorites.toSong
import fr.francheze.domain.playSong.PlaySongUseCase
import fr.francheze.melodize.common.Event
import fr.francheze.melodize.common.postEvent
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class FavoriteViewModel @Inject constructor(
    private val updateFavoritesUseCase: UpdateFavoritesUseCase,
    private val playSongUseCase: PlaySongUseCase
) : ViewModel() {
    val favoriteLiveData = MutableLiveData<List<Favorite>>()
    val errorLiveData = MutableLiveData<Event<Throwable>>()
    val playerRedirectLiveData = MutableLiveData<Event<Boolean>>()

    fun getAllFavorites() {
        viewModelScope.launch {
            kotlin.runCatching {
                val favoriteList = updateFavoritesUseCase.getAllFavorites()
                favoriteLiveData.postValue(favoriteList)
            }.onFailure {
                errorLiveData.postEvent(it)
            }
        }
    }

    fun deleteFavorites(favoriteList: List<Favorite>) {
        viewModelScope.launch {
            kotlin.runCatching {
                updateFavoritesUseCase.deleteFavoriteList(favoriteList)
                getAllFavorites()
            }.onFailure {
                errorLiveData.postEvent(it)
            }
        }
    }

    fun setPlayingSong(favorite: Favorite) {
        viewModelScope.launch {
            kotlin.runCatching {
                playSongUseCase.setPlayingSong(favorite.toSong())
                playerRedirectLiveData.postEvent(true)
            }.onFailure {
                Timber.e(it)
                playerRedirectLiveData.postEvent(false)
            }
        }
    }
}