package fr.francheze.melodize.favorites

import android.content.res.Configuration
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import fr.francheze.domain.favorites.Favorite
import fr.francheze.melodize.R
import fr.francheze.melodize.common.Event
import fr.francheze.melodize.common.FavoriteRecyclerAdapter
import fr.francheze.melodize.common.FavoriteSelectedListener
import fr.francheze.melodize.common.MultiSelectionModeListener
import fr.francheze.melodize.databinding.FragmentFavoritesBinding
import fr.francheze.melodize.main.MainActivity
import timber.log.Timber
import javax.inject.Inject

@AndroidEntryPoint
class FavoritesFragment @Inject constructor() : Fragment(), FavoriteSelectedListener, MultiSelectionModeListener {

    private val favoriteViewModel: FavoriteViewModel by activityViewModels()

    private var binding: FragmentFavoritesBinding? = null
    private var favoriteListAdapter: FavoriteRecyclerAdapter? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentFavoritesBinding.inflate(inflater, container, false)
        val view = binding?.root

        val layoutManager = if (requireActivity().resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT) {
            GridLayoutManager(requireContext(), 2)
        } else {
            GridLayoutManager(requireContext(), 4)
        }
        binding?.songList?.layoutManager = layoutManager
        favoriteListAdapter = FavoriteRecyclerAdapter(mutableListOf(), this, this)
        favoriteListAdapter?.stateRestorationPolicy = RecyclerView.Adapter.StateRestorationPolicy.PREVENT_WHEN_EMPTY
        binding?.songList?.adapter = favoriteListAdapter

        binding?.deleteButton?.setOnClickListener { deleteSelectedFavorites() }

        addOnBackPressedBehaviour()

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        favoriteViewModel.favoriteLiveData.observe(viewLifecycleOwner, ::updateFavoriteList)
        favoriteViewModel.errorLiveData.observe(viewLifecycleOwner, ::displayErrorMessage)
        favoriteViewModel.playerRedirectLiveData.observe(viewLifecycleOwner) {
            it.getContentIfNotHandled()?.let {
                (activity as MainActivity).goToPlayerFragment()
            }
        }

        favoriteViewModel.getAllFavorites()
    }

    private fun deleteSelectedFavorites() {
        favoriteListAdapter?.selectedFavorites?.values?.toList()?.let {
            favoriteViewModel.deleteFavorites(it)
            clearSelection()
        } ?: kotlin.run {
            Snackbar.make(requireView(), R.string.favoriteDeleteErrorMessage, Snackbar.LENGTH_SHORT).show()
        }
    }

    private fun updateFavoriteList(songList: List<Favorite>) {
        toggleEmptyListPlaceholder(songList.isEmpty())
        if (songList.isNotEmpty()) {
            favoriteListAdapter?.favoriteList?.clear()
            favoriteListAdapter?.favoriteList?.addAll(songList)
            favoriteListAdapter?.notifyDataSetChanged()
        }
    }

    private fun displayErrorMessage(error: Event<Throwable>) {
        Timber.e(error.getContentIfNotHandled())
        toggleEmptyListPlaceholder(favoriteListAdapter?.itemCount == 0)
        if (this.isResumed && view != null) {
            Snackbar.make(requireView(), R.string.favoriteErrorMessage, Snackbar.LENGTH_SHORT).show()
        }
    }

    private fun clearSelection() {
        favoriteListAdapter?.toggleMultiSelectionMode(false)
        favoriteListAdapter?.selectedFavorites?.clear()
        favoriteListAdapter?.notifyDataSetChanged()
    }

    private fun toggleEmptyListPlaceholder(isVisible: Boolean) {
        binding?.emptyListPlaceholder?.isVisible = isVisible
        binding?.songList?.isVisible = !isVisible
    }

    override fun onFavoriteSelected(favorite: Favorite) {
        favoriteViewModel.setPlayingSong(favorite)
    }

    override fun onMultiSelectionModeChanged(isInMultiSelectionMode: Boolean) {
        binding?.deleteButton?.isVisible = isInMultiSelectionMode
    }

    private fun addOnBackPressedBehaviour() {
        requireActivity()
                .onBackPressedDispatcher
                .addCallback(viewLifecycleOwner, object : OnBackPressedCallback(true) {
                    override fun handleOnBackPressed() {
                        if (favoriteListAdapter?.isInMultiSelectionMode == true) {
                            clearSelection()
                        } else {
                            requireActivity().finish()
                        }
                    }
                }
                )
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }
}