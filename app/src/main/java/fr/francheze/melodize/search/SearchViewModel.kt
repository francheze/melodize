package fr.francheze.melodize.search

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import fr.francheze.domain.favorites.UpdateFavoritesUseCase
import fr.francheze.domain.favorites.toFavorite
import fr.francheze.domain.playSong.PlaySongUseCase
import fr.francheze.domain.songs.Song
import fr.francheze.domain.songs.UpdateSongsUseCase
import fr.francheze.melodize.common.Event
import fr.francheze.melodize.common.postEvent
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class SearchViewModel @Inject constructor(
    private val updateSongsUseCase: UpdateSongsUseCase,
    private val updateFavoritesUseCase: UpdateFavoritesUseCase,
    private val playSongUseCase: PlaySongUseCase
) : ViewModel() {
    val songLiveData = MutableLiveData<List<Song>>()
    val errorLiveData = MutableLiveData<Event<Throwable>>()
    val favoriteUpdateErrorLiveData = MutableLiveData<Event<Song>>()
    val playerRedirectLiveData = MutableLiveData<Event<Boolean>>()

    fun searchSongWithTitle(title: String) {
        viewModelScope.launch {
            kotlin.runCatching {
                val songList = updateSongsUseCase.searchSongsByTitle(title.lowercase())
                songLiveData.postValue(songList)
            }.onFailure {
                errorLiveData.postEvent(it)
            }
        }
    }

    fun addFavorite(song: Song) {
        viewModelScope.launch {
            kotlin.runCatching {
                updateFavoritesUseCase.addFavorite(song)
            }.onFailure {
                favoriteUpdateErrorLiveData.postEvent(song)
            }
        }
    }

    fun removeFavorite(song: Song) {
        viewModelScope.launch {
            kotlin.runCatching {
                updateFavoritesUseCase.deleteFavorite(song.toFavorite())
            }.onFailure {
                favoriteUpdateErrorLiveData.postEvent(song)
            }
        }
    }

    fun setPlayingSong(song: Song) {
        viewModelScope.launch {
            kotlin.runCatching {
                playSongUseCase.setPlayingSong(song)
                playerRedirectLiveData.postEvent(true)
            }.onFailure {
                Timber.e(it)
                playerRedirectLiveData.postEvent(false)
            }
        }
    }
}