package fr.francheze.melodize.search

import android.content.res.Configuration
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import fr.francheze.domain.songs.Song
import fr.francheze.melodize.R
import fr.francheze.melodize.common.Event
import fr.francheze.melodize.common.FavoriteListener
import fr.francheze.melodize.common.SongRecyclerAdapter
import fr.francheze.melodize.common.SongSelectedListener
import fr.francheze.melodize.databinding.FragmentSearchBinding
import fr.francheze.melodize.main.MainActivity
import timber.log.Timber
import javax.inject.Inject

@AndroidEntryPoint
class SearchFragment @Inject constructor() : Fragment(), SongSelectedListener, FavoriteListener {

    private val searchViewModel: SearchViewModel by activityViewModels()

    private var binding: FragmentSearchBinding? = null
    private var songListAdapter: SongRecyclerAdapter? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentSearchBinding.inflate(inflater, container, false)
        val view = binding?.root

        val layoutManager = if (requireActivity().resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT) {
            GridLayoutManager(requireContext(), 2)
        } else {
            GridLayoutManager(requireContext(), 4)
        }
        binding?.songList?.layoutManager = layoutManager
        songListAdapter = SongRecyclerAdapter(mutableListOf(), this, this)
        songListAdapter?.stateRestorationPolicy = RecyclerView.Adapter.StateRestorationPolicy.PREVENT_WHEN_EMPTY
        binding?.songList?.adapter = songListAdapter

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        searchViewModel.songLiveData.observe(viewLifecycleOwner, ::updateSongList)
        searchViewModel.errorLiveData.observe(viewLifecycleOwner, ::displayErrorMessage)
        searchViewModel.favoriteUpdateErrorLiveData.observe(viewLifecycleOwner, ::displayFavoriteError)
        searchViewModel.playerRedirectLiveData.observe(viewLifecycleOwner) {
            it.getContentIfNotHandled()?.let {
                (activity as MainActivity).goToPlayerFragment()
            }
        }

        binding?.searchInputEditText?.doOnTextChanged { text, start, before, count ->
            searchViewModel.searchSongWithTitle(text?.toString() ?: "")
        }
    }

    private fun updateSongList(songList: List<Song>) {
        toggleEmptyListPlaceholder(songList.isEmpty())
        if (songList.isNotEmpty()) {
            songListAdapter?.songs?.clear()
            songListAdapter?.songs?.addAll(songList)
            songListAdapter?.notifyDataSetChanged()
        }
    }

    private fun displayErrorMessage(error: Event<Throwable>) {
        Timber.e(error.getContentIfNotHandled())
        toggleEmptyListPlaceholder(songListAdapter?.itemCount == 0)
        if (this.isResumed && view != null) {
            Snackbar.make(requireView(), R.string.searchErrorMessage, Snackbar.LENGTH_SHORT).show()
        }
    }

    private fun displayFavoriteError(song: Event<Song>) {
        song.getContentIfNotHandled()?.let { songInError ->
            songListAdapter?.songs?.indexOf(songInError)?.let { songPositionInList ->
                songListAdapter?.songs?.set(songPositionInList, songInError)
                songListAdapter?.notifyItemChanged(songPositionInList)
            }
            if (this.isResumed && view != null) {
                Snackbar.make(requireView(), R.string.favoriteAddError, Snackbar.LENGTH_SHORT).show()
            }
        }
    }

    private fun toggleEmptyListPlaceholder(isVisible: Boolean) {
        binding?.emptyListPlaceholder?.isVisible = isVisible
        binding?.songList?.isVisible = !isVisible
    }

    override fun onSongSelected(song: Song) {
        searchViewModel.setPlayingSong(song)
    }

    override fun addAsFavorite(song: Song, isFavorite: Boolean) {
        if (isFavorite) {
            searchViewModel.addFavorite(song)
        } else {
            searchViewModel.removeFavorite(song)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }
}