package fr.francheze.melodize.player

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import fr.francheze.domain.playSong.PlaySongUseCase
import fr.francheze.domain.songs.Song
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class PlayerViewModel @Inject constructor(
    private val playSongUseCase: PlaySongUseCase,
) : ViewModel() {
    val songLiveData = MutableLiveData<Song?>()
    val playStateLiveData = MutableLiveData(false)

    fun togglePlayState(forcedState: Boolean? = null) {
        playStateLiveData.postValue(
            forcedState ?: playStateLiveData.value?.not() ?: true
        )
    }

    fun getPlayingSong() {
        viewModelScope.launch {
            kotlin.runCatching {
                val playingSong = playSongUseCase.getPlayingSong()
                songLiveData.postValue(playingSong)
            }.onFailure {
                Timber.e(it)
                songLiveData.postValue(null)
            }
        }
    }
}