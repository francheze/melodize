package fr.francheze.melodize.player

import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import dagger.hilt.android.AndroidEntryPoint
import fr.francheze.domain.songs.Song
import fr.francheze.melodize.R
import fr.francheze.melodize.common.utils.ImageProcessingUtils.addRandomColorBackground
import fr.francheze.melodize.common.utils.ImageProcessingUtils.loadImage
import fr.francheze.melodize.databinding.FragmentPlayerBinding
import timber.log.Timber
import javax.inject.Inject

@AndroidEntryPoint
class PlayerFragment @Inject constructor() : Fragment(), RequestListener<Drawable> {

    private val playerViewModel: PlayerViewModel by activityViewModels()

    private var binding: FragmentPlayerBinding? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentPlayerBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding?.playButton?.setOnClickListener { playerViewModel.togglePlayState() }

        playerViewModel.songLiveData.observe(viewLifecycleOwner, ::setSong)
        playerViewModel.playStateLiveData.observe(viewLifecycleOwner, ::toggleMusic)

        playerViewModel.getPlayingSong()
    }

    private fun setSong(song: Song?) {
        song?.let {
            binding?.albumCover?.loadImage(song.url, this)
            binding?.songTitle?.text = song.title
            binding?.playButton?.isEnabled = true
            playerViewModel.togglePlayState(true)
        } ?: kotlin.run {
            binding?.albumCover?.addRandomColorBackground()
            binding?.albumCover?.setImageResource(R.drawable.ic_album)
            binding?.playButton?.isEnabled = false
        }
    }

    private fun toggleMusic(playMusic: Boolean) {
        binding?.equalizerAnimation?.let {
            if (playMusic) it.playAnimation() else it.pauseAnimation()
        }
        binding?.playButton?.apply {
            setImageResource(if (playMusic) R.drawable.ic_pause else R.drawable.ic_play)
            contentDescription = getString(if (playMusic) R.string.pauseButtonDescription else R.string.playButtonDescription)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }

    override fun onLoadFailed(
        e: GlideException?,
        model: Any?,
        target: Target<Drawable>?,
        isFirstResource: Boolean
    ): Boolean {
        Timber.e(e)
        binding?.albumCover?.addRandomColorBackground()
        binding?.albumCover?.setImageResource(R.drawable.ic_album)
        return false
    }

    override fun onResourceReady(
        resource: Drawable?,
        model: Any?,
        target: Target<Drawable>?,
        dataSource: DataSource?,
        isFirstResource: Boolean
    ) = false
}