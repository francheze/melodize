plugins {
    id("com.android.application")
    kotlin("android")
    id("kotlin-parcelize")
    kotlin("kapt")
    id("dagger.hilt.android.plugin")
}

android {
    compileSdk = Dependencies.Android.compileSdkVersion
    defaultConfig {
        applicationId = "fr.francheze.melodize"
        minSdk = Dependencies.Android.minSdkVersion
        targetSdk = Dependencies.Android.targetSdkVersion
        versionCode = 1
        versionName = "1.0"
        testInstrumentationRunner = "android.support.test.runner.AndroidJUnitRunner"
    }

    signingConfigs {
        create("release") {
            storeFile = file("../keystore/release.jks")
            storePassword = "releaseKey"
            keyAlias = "releaseKey"
            keyPassword = "releaseKey"
        }
    }

    buildTypes {
        getByName("release") {
            signingConfig = signingConfigs.getByName("release")
            isDebuggable = false
            /*isMinifyEnabled  = true // TODO Need more time to adjust Proguard file
            isShrinkResources = true*/
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
        }
        getByName("debug") {
            isDebuggable = true
            isMinifyEnabled = false
        }
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    kotlinOptions {
        jvmTarget = "1.8"
    }

    buildFeatures {
        viewBinding = true
    }
}

dependencies {
    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))

    implementation(Dependencies.Android.androidxAppCompat)
    implementation(Dependencies.Android.androidxFragment)
    implementation(Dependencies.Android.androidxCore)

    implementation(Dependencies.Ktx.kotlinReflect)
    implementation(Dependencies.Ktx.stdlib)

    implementation(Dependencies.Ui.materialAndroid)
    implementation(Dependencies.Ui.constraintLayout)
    implementation(Dependencies.Ui.recyclerView)
    implementation(Dependencies.Ui.swipeRefreshLayout)
    implementation(Dependencies.Ui.lottie)

    implementation(Dependencies.Hilt.hiltAndroid)
    kapt(Dependencies.Hilt.hiltCompiler)

    implementation(Dependencies.Retrofit.retrofit)

    implementation(Dependencies.Room.roomKtx)
    
    implementation(Dependencies.Log.timber)

    implementation(Dependencies.Lifecycle.lifecycleLiveData)
    implementation(Dependencies.Lifecycle.lifecycleExtensions)
    implementation(Dependencies.Lifecycle.lifecycleReactiveStreams)
    implementation(Dependencies.Lifecycle.lifecycleReactiveStreamsExtensions)

    androidTestImplementation(Dependencies.Test.androidJunit)
    androidTestImplementation(Dependencies.Test.espresso)
    testImplementation(Dependencies.Test.junit)
    testImplementation(Dependencies.Test.mockito)
    testImplementation(Dependencies.Test.mockitoInline)
    testImplementation(Dependencies.Test.assertj)
    testImplementation(Dependencies.Test.coreTesting)

    implementation(Dependencies.Glide.glide)
    kapt(Dependencies.Glide.glideCompiler)

    implementation(project(":domain"))
    implementation(project(":data"))
}

kapt {
    correctErrorTypes = true
}