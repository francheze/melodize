package fr.francheze.domain.songs

import fr.francheze.data.favorites.FavoriteEntity
import fr.francheze.data.favorites.FavoriteRepositoryImpl
import fr.francheze.data.songs.SongRepositoryImpl
import junit.framework.TestCase
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class UpdateSongsUseCaseTest : TestCase() {

    @Mock
    private lateinit var songRepository: SongRepositoryImpl
    @Mock
    private lateinit var favoriteRepository: FavoriteRepositoryImpl

    private lateinit var updateSongsUseCase: UpdateSongsUseCase
    private lateinit var favoriteList: List<FavoriteEntity>
    private lateinit var songList: List<Song>

    @Before
    fun beforeTest() {
        favoriteList = listOf(
            FavoriteEntity(1, 10, "a", "urlA", "thumbnailUrlA"),
            FavoriteEntity(3, 30, "c", "urlC", "thumbnailUrlC"),
            FavoriteEntity(4, 40, "d", "urlD", "thumbnailUrlD"),
            FavoriteEntity(6, 60, "f", "urlF", "thumbnailUrlF")
        )

        songList = listOf(
            Song(1, 10, "a", "urlA", "thumbnailUrlA", false),
            Song(2, 20, "b", "urlB", "thumbnailUrlB", false),
            Song(3, 30, "c", "urlC", "thumbnailUrlC", false),
            Song(4, 40, "d", "urlD", "thumbnailUrlD", false),
            Song(5, 50, "e", "urlE", "thumbnailUrlE", false),
            Song(6, 60, "f", "urlF", "thumbnailUrlF", false)
        )

        updateSongsUseCase = UpdateSongsUseCase(songRepository, favoriteRepository)
    }

    @Test
    fun `test that favorites are properly added to the list when calling mergeFavoriteWithSongs`() {
        //Given
        val favorites = favoriteList
        val songs = songList
        val useCase = updateSongsUseCase

        //When
        val mergedList = useCase.mergeFavoriteWithSongs(favorites, songs)

        //Assert
        assert(mergedList.first { it.id == 1 }.isFavorite)
        assert(mergedList.first { it.id == 2 }.isFavorite.not())
        assert(mergedList.first { it.id == 3 }.isFavorite)
        assert(mergedList.first { it.id == 4 }.isFavorite)
        assert(mergedList.first { it.id == 5 }.isFavorite.not())
        assert(mergedList.first { it.id == 6 }.isFavorite)
    }
}