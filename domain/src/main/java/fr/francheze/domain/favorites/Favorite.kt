package fr.francheze.domain.favorites

import android.os.Parcelable
import fr.francheze.data.favorites.FavoriteEntity
import fr.francheze.domain.songs.Song
import kotlinx.parcelize.Parcelize

@Parcelize
data class Favorite(
    val id: Int,
    val albumId: Int?,
    val title: String?,
    val url: String?,
    val thumbnailUrl: String?,
) : Parcelable

fun Song.toFavorite() = Favorite(
    id = id,
    albumId = albumId,
    title = title,
    url = url,
    thumbnailUrl = thumbnailUrl
)

fun Favorite.toSong() = Song(
    id = id,
    albumId = albumId,
    title = title,
    url = url,
    thumbnailUrl = thumbnailUrl,
    isFavorite = true
)

fun FavoriteEntity.toFavorite() = Favorite(
    id = id,
    albumId = albumId,
    title = title,
    url = url,
    thumbnailUrl = thumbnailUrl
)

fun Favorite.toFavoriteEntity() = FavoriteEntity(
    id = id,
    albumId = albumId,
    title = title,
    url = url,
    thumbnailUrl = thumbnailUrl
)