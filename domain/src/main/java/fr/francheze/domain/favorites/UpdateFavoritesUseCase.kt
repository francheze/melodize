package fr.francheze.domain.favorites

import fr.francheze.data.favorites.FavoriteRepositoryImpl
import fr.francheze.domain.songs.Song
import javax.inject.Inject

class UpdateFavoritesUseCase @Inject constructor(private val favoriteRepository: FavoriteRepositoryImpl) {

    suspend fun getAllFavorites(): List<Favorite> = favoriteRepository.getAllFavorites().map { it.toFavorite() }

    suspend fun addFavorite(song: Song) = favoriteRepository.addFavorite(song.toFavorite().toFavoriteEntity())

    suspend fun deleteFavoriteList(favoriteList: List<Favorite>) =
        favoriteRepository.deleteFavoriteList(favoriteList.map { it.toFavoriteEntity() })

    suspend fun deleteFavorite(favorite: Favorite) =
        favoriteRepository.deleteFavorite(favorite.toFavoriteEntity())
}