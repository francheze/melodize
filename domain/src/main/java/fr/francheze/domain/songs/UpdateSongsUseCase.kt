package fr.francheze.domain.songs

import androidx.annotation.VisibleForTesting
import fr.francheze.data.favorites.FavoriteEntity
import fr.francheze.data.favorites.FavoriteRepositoryImpl
import fr.francheze.data.songs.SongRepositoryImpl
import fr.francheze.domain.favorites.Favorite
import javax.inject.Inject

class UpdateSongsUseCase @Inject constructor(
    private val songRepository: SongRepositoryImpl,
    private val favoriteRepository: FavoriteRepositoryImpl
) {
    suspend fun refreshSongs(): List<Song> {
        val response = songRepository.refreshSongs()
        return if (response.isSuccessful) {
            val songs = response.body().orEmpty().map { it.toSong() }
            songRepository.saveSongs(songs.map { it.toSongEntity() })
            mergeFavoriteWithSongs(favoriteRepository.getAllFavorites(), songs)
        } else {
            emptyList()
        }
    }

    suspend fun getSongs(): List<Song> = songRepository.getAllSongs()
            .map { it.toSong() }
            .let { mergeFavoriteWithSongs(favoriteRepository.getAllFavorites(), it) }
            .ifEmpty {
                refreshSongs()
            }

    suspend fun searchSongsByTitle(title: String): List<Song> =
        songRepository.searchSongsByTitle(title).map { it.toSong() }

    @VisibleForTesting
    internal fun mergeFavoriteWithSongs(favoriteList: List<FavoriteEntity>, songList: List<Song>): List<Song> {
        val favoriteIds = favoriteList.map { it.id }
        return songList.map { it.apply { isFavorite = favoriteIds.contains(it.id) } }
    }
}