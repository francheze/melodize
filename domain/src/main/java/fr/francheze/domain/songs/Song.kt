package fr.francheze.domain.songs

import android.os.Parcelable
import fr.francheze.data.play.PlayingSong
import fr.francheze.data.songs.SongEntity
import fr.francheze.data.songs.SongResponse
import kotlinx.parcelize.Parcelize

@Parcelize
data class Song(
    val id: Int,
    val albumId: Int?,
    val title: String?,
    val url: String?,
    val thumbnailUrl: String?,
    var isFavorite: Boolean
) : Parcelable

fun SongResponse.toSong() = Song(
    id = id,
    albumId = albumId,
    title = title,
    url = url,
    thumbnailUrl = thumbnailUrl,
    isFavorite = false
)

fun SongEntity.toSong() = Song(
    id = id,
    albumId = albumId,
    title = title,
    url = url,
    thumbnailUrl = thumbnailUrl,
    isFavorite = false
)

fun Song.toSongEntity() = SongEntity(
    id = id,
    albumId = albumId,
    title = title,
    url = url,
    thumbnailUrl = thumbnailUrl
)

fun PlayingSong.toSong() = Song(
    id = id,
    albumId = albumId,
    title = title,
    url = url,
    thumbnailUrl = thumbnailUrl,
    isFavorite = false
)

fun Song.toPlayingSong() = PlayingSong(
    id = id,
    albumId = albumId,
    title = title,
    url = url,
    thumbnailUrl = thumbnailUrl,
    isFavorite = false
)