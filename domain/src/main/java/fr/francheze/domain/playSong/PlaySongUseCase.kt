package fr.francheze.domain.playSong

import fr.francheze.data.play.PlayRepositoryImpl
import fr.francheze.domain.songs.Song
import fr.francheze.domain.songs.toPlayingSong
import fr.francheze.domain.songs.toSong
import javax.inject.Inject

class PlaySongUseCase @Inject constructor(
    private val playRepository: PlayRepositoryImpl,
) {
    suspend fun getPlayingSong(): Song? = playRepository.getPlayingSong()?.toSong()

    suspend fun setPlayingSong(song: Song) {
        playRepository.setPlayingSong(song.toPlayingSong())
    }
}