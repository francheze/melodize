package fr.francheze.data.utils

object Constants {
    // Network
    const val BASE_LEBONCOIN_URL = "https://static.leboncoin.fr/"

    // Database
    const val DATABASE_VERSION = 2
    const val DATABASE_NAME = "Melodize_Database"

    // Shared preferences
    const val PLAYING_SONG_KEY = "PLAYING_SONG"
}