package fr.francheze.data.favorites

import javax.inject.Inject

interface FavoriteRepository {
    suspend fun getAllFavorites(): List<FavoriteEntity>

    suspend fun addFavorite(favorite: FavoriteEntity)

    suspend fun addFavoriteList(favoriteList: List<FavoriteEntity>)

    suspend fun deleteFavorite(favorite: FavoriteEntity)

    suspend fun deleteFavoriteList(favoriteList: List<FavoriteEntity>)
}

class FavoriteRepositoryImpl @Inject constructor(
    private val favoriteDao: FavoriteDao
) : FavoriteRepository {
    override suspend fun getAllFavorites(): List<FavoriteEntity> = favoriteDao.getAll()

    override suspend fun addFavorite(favorite: FavoriteEntity) = favoriteDao.insert(favorite)

    override suspend fun addFavoriteList(favoriteList: List<FavoriteEntity>) = favoriteDao.insertList(favoriteList)

    override suspend fun deleteFavorite(favorite: FavoriteEntity) = favoriteDao.delete(favorite)

    override suspend fun deleteFavoriteList(favoriteList: List<FavoriteEntity>) = favoriteDao.deleteList(favoriteList)

}