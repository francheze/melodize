package fr.francheze.data.favorites

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface FavoriteDao {
    @Query("SELECT * FROM favorite")
    suspend fun getAll(): List<FavoriteEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(favorite: FavoriteEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertList(favoriteList: List<FavoriteEntity>)

    @Delete
    suspend fun delete(favorite: FavoriteEntity)

    @Delete
    suspend fun deleteList(favoriteList: List<FavoriteEntity>)
}