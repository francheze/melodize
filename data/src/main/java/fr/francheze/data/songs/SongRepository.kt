package fr.francheze.data.songs

import retrofit2.Response
import javax.inject.Inject

interface SongRepository {
    suspend fun refreshSongs(): Response<List<SongResponse>>

    suspend fun getAllSongs(): List<SongEntity>

    suspend fun getAllSongsFromIds(songIds: IntArray): List<SongEntity>

    suspend fun searchSongsByTitle(title: String): List<SongEntity>

    suspend fun saveSongs(songs: List<SongEntity>)
}

class SongRepositoryImpl @Inject constructor(
    private val songApi: SongApi,
    private val songDAO: SongDao,
    ): SongRepository {

    override suspend fun refreshSongs(): Response<List<SongResponse>> = songApi.getAllSongs()

    override suspend fun getAllSongs(): List<SongEntity> = songDAO.getAll()

    override suspend fun getAllSongsFromIds(songIds: IntArray): List<SongEntity> = songDAO.getAllFromIds(songIds)

    override suspend fun searchSongsByTitle(title: String): List<SongEntity> = songDAO.findByTitle(title)

    override suspend fun saveSongs(songs: List<SongEntity>) = songDAO.insertAll(songs)
}