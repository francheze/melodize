package fr.francheze.data.songs

data class SongResponse(
    val id: Int,
    val albumId: Int? = 0,
    val title: String? = null,
    val url: String? = null,
    val thumbnailUrl: String ? = ""
)