package fr.francheze.data.songs

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface SongDao {
    @Query("SELECT * FROM song")
    suspend fun getAll(): List<SongEntity>

    @Query("SELECT * FROM song WHERE id IN (:songIds)")
    suspend fun getAllFromIds(songIds: IntArray): List<SongEntity>

    @Query("SELECT * FROM song WHERE title LIKE '%' || :title || '%'")
    suspend fun findByTitle(title: String): List<SongEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(song: SongEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(songs: List<SongEntity>)

    @Delete
    suspend fun delete(user: SongEntity)
}