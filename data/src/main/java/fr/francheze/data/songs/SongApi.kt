package fr.francheze.data.songs

import fr.francheze.data.songs.SongResponse
import retrofit2.Response
import retrofit2.http.GET

interface SongApi {

    @GET("img/shared/technical-test.json")
    suspend fun getAllSongs(): Response<List<SongResponse>>
}