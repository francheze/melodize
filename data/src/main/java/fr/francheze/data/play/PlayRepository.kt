package fr.francheze.data.play

import android.content.SharedPreferences
import fr.francheze.data.utils.Constants.PLAYING_SONG_KEY
import javax.inject.Inject

interface PlayRepository {
    suspend fun getPlayingSong(): PlayingSong?

    suspend fun setPlayingSong(song: PlayingSong)
}

class PlayRepositoryImpl @Inject constructor(
    private val sharedPreferences: SharedPreferences,
) : PlayRepository {
    override suspend fun getPlayingSong(): PlayingSong? =
        sharedPreferences.getString(PLAYING_SONG_KEY, null)?.fromPlayingSongJson()

    override suspend fun setPlayingSong(song: PlayingSong) {
        sharedPreferences.edit().putString(PLAYING_SONG_KEY, song.toJson()).apply()
    }
}