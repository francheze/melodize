package fr.francheze.data.play

import android.os.Parcelable
import com.google.gson.Gson
import kotlinx.parcelize.Parcelize

@Parcelize
data class PlayingSong(
    val id: Int,
    val albumId: Int?,
    val title: String?,
    val url: String?,
    val thumbnailUrl: String?,
    var isFavorite: Boolean
) : Parcelable

fun PlayingSong.toJson(): String = Gson().toJson(this)
fun String.fromPlayingSongJson(): PlayingSong = Gson().fromJson(this, PlayingSong::class.java)