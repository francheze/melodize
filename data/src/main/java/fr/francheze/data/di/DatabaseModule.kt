package fr.francheze.data.di

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import androidx.room.Room
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import fr.francheze.data.database.AppDatabase
import fr.francheze.data.favorites.FavoriteDao
import fr.francheze.data.songs.SongDao
import fr.francheze.data.utils.Constants.DATABASE_NAME
import fr.francheze.data.utils.Constants.DATABASE_VERSION
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class DatabaseModule {
    @Provides
    @Singleton
    fun provideAppDatabase(@ApplicationContext appContext: Context): AppDatabase {
        var needToMigrate = false
        kotlin.runCatching { //Catch to prevent crash on first database creation
            val db = SQLiteDatabase.openDatabase(appContext.getDatabasePath(DATABASE_NAME).path, null, SQLiteDatabase.OPEN_READONLY)
            needToMigrate = db.version < DATABASE_VERSION
        }
        return Room
                .databaseBuilder(
                    appContext,
                    AppDatabase::class.java,
                    DATABASE_NAME
                )
                .allowMainThreadQueries().apply {
                    if (needToMigrate) this.fallbackToDestructiveMigration()
                }
                .build()
    }

    @Provides
    fun provideSongDao(appDatabase: AppDatabase): SongDao {
        return appDatabase.songDao()
    }

    @Provides
    fun provideFavoriteDao(appDatabase: AppDatabase): FavoriteDao {
        return appDatabase.favoriteDao()
    }
}