package fr.francheze.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import fr.francheze.data.favorites.FavoriteDao
import fr.francheze.data.favorites.FavoriteEntity
import fr.francheze.data.songs.SongDao
import fr.francheze.data.songs.SongEntity
import fr.francheze.data.utils.Constants.DATABASE_VERSION

@Database(entities = [SongEntity::class, FavoriteEntity::class], version = DATABASE_VERSION)
abstract class AppDatabase : RoomDatabase() {
    abstract fun songDao(): SongDao
    abstract fun favoriteDao(): FavoriteDao
}