# Melodize
## Rappel de l’exercice :

Réaliser une application native Android affichant la liste des albums suivant :

[https://static.leboncoin.fr/img/shared/technical-test.json](https://static.leboncoin.fr/img/shared/technical-test.json)

Les données contenues dans ce json doivent être téléchargées par l'app au Runtime, et non mises manuellement dans les assets de l'app.

- Le projet est à réaliser sur la plateforme Android (API minimum 21)

- Le code doit être du Kotlin et tu devras implémenter un système de persistance des données afin que les données puissent être disponibles offline, même après redémarrage de l'application.

- Liberté d'utiliser les librairies que tu souhaites, mais tu devras expliquer tes choix.

- Le code doit être versionné sur un dépôt Git librement consultable

  
  

## Projet

- Version mini : 21, c'est à dire Android 5. On cible à peu près 96% des devices Android, tout en évitant de trop se limiter dans les fonctionnalités Android

- Version max : 31, c'est à dire Android 12 qui sortira bientôt en release. Cela permet d'anticiper sa sortie et d'éviter la dette technique.

Il y a deux buildTypes dans l'applications :

- "debug" pour la version de test sans traitement sur le code

- "release" avec signature spécifique et desactivation du mode debug

J'ai ajouté les fichiers .APK dans le dossier [/apk](/apk)
 pour faciliter le test de l'application :


  

## Technos

### Kotlin

Par rapport à Java, Kotlin ne présente que des avantages :

- C'est le langage maintenant préconisé par Google

- Il permet bien plus de choses que Java (en tout cas par rapport aux versions de Java disponible avec les anciennes versions d'Android), comme par exemple les extensions, les coroutines, la programmation fonctionnelle avec les listes, etc ...

- Il est moins "verbeux" et donc permet d'écrire du code plus lisible et léger

### MaterialAndroid

Cette librairie permet d'utiliser les composants graphiques qui correspondent aux guidelines du Material design, et ainsi être en adéquation avec le design Android

### ConstraintLayout

ConstraintLayout est un des types de layouts disponibles dans Android, et qui permet d'organiser des éléments de vues à l'aide de contraintes.

Il a l'avantage d'être assez intuitif d'utilisation, et surtout de donner beaucoup plus de liberté d'organisation que les autres types de layout.

### RecyclerView

Il y a un nombre important de chansons à afficher au sein des différentes listes de l'application

De plus, la taille mémoire de chaque élément n'est pas négligeable à cause de l'image de la chanson.

L'utilisation d'une RecyclerView est donc nécessaire pour garantir la performance du scroll de la vue grâce au recyclage de vue.

### SwipeRefreshLayout

Ce composant permet d'avoir un système intuitif via swipe pour demander un rafraichissement de la liste de chansons.

Ce fonctionnement est en place sur la plupart des outils de Google (recherche de nouveaux mails sur GMail, rafraichissement de page sur Google Chrome, ...) et ne devrait donc pas poser de problème de compréhension pour l'utilisateur.

### Lifecycle (LiveData & ViewModel)

Grâce à ces librairies, il est possible de mettre en place une architecture MVVM.

Les ViewModel permettent de stocker les données de la vue de manière efficace.

Les LiveData permettent à la vue de récupérer des données du ViewModel tout en évitant au ViewModel de garder une référence de la vue (ce serait un gros risque de fuite mémoire).

### Databinding

Depuis que les kotlin-extensions sont deprecated, le databinding est la solution de prédilection pour binder des éléments de vue au code des Activités et Fragments.

De plus cela permet de ne pas s'abstreindre à utiliser "findViewById", qui est une méthode peu performantes et génère pas mal de boilerplate.

### Hilt

Tout projet de grande envergure se doit d'utiliser de l'injection de dépendance.

Cela permet de faciliter grandement la mise en place de tests.

Cela permet aussi à d'autre développeurs d'utiliser plus facilement ce que les autres ont développés (pas besoin de savoir quoi fournir aux constructeurs).

Le choix de Hilt en particulier s'est fait car c'est la librairie de prédilection préconisée par Google pour Kotlin (la remplaçante de Dagger, qui se base grandement sur celui ci d'ailleurs)

### Timber

Cette petite librairie du fameux Jake Wharton est bien pratique pour pouvoir effectuer des logs applications sans avoir à écrire du code boilerplate pour l'ajout de tags.

### Coroutines

J'ai hésité lors de la mise en place du projet à utiliser RxAndroid que je maitrise mieux.

Cependant, je pense que les Coroutines sont une solution plus pérenne car poussées par Google désormais.

C'est aussi bien plus lisible que du Rx et plus facile à debugger.

### Retrofit

La librairie de prédilection, développée par Square, pour la gestion des appels d'API.

Elle facilite grandement la gestions d'API en permettant notamment :

- la définition sans boilerplate d'appels d'API

- une gestion assez simple des réponses aux appels (et bien intégré avec les Coroutines)

- la mise en place de mock d'api sans avoir besoin de grosses modifications

- le log des appels d'api

### JUnit & Mockito & Espresso

Ce sont les librairies de bases proposées par Android pour faire du test unitaire, d'intégration et UI.


### AssertJ

Petite librairie de Square permettant de faciliter la mise en place de tests unitaire avec un large pannel de méthodes de verification.

  

### Room

Pour la gestion des bases de données, il existe pas mal de librairies différentes qui utilise des technologies différentes (SQL, NoSQL, SQLite, etc ...).

Pour cette application, j'ai choisi Room pour deux raisons :

- elle fait partie des Architecture Components d'Android, et cela garantie sa bonne intégration au reste des technologies choisie pour ce projet

- c'est du SQLite, et donc cela garantie un bon niveau de performance lors des différentes requêtes, tout en étant facile à mettre en place

### Glide

Télécharger les images des musiques tout en gérant le cache est une tache complexe.

Le passage par l'utilisation d'une librairie est quasi

### Lottie
La librairie d'animation de AirBnb, très simple de mise en place et très performante.
Cela permet de mettre de belles animations dans une application sans vraie complication côté code.
Et côté designers, l'import d'animations sur Lottie est relativement simple aussi.
  

## Architecture

### MVVM

L'avantage principal de cette architecture par rapport aux autres, est qu'elle s'intègre parfaitement à la gestion des cycles de vie des Activités et Fragments.

Lorsque le Fragment est détruit, le ViewModel persiste et permet de garder les données de la vue.

Ainsi, lors de la rotation de l'écran par exemple, le Fragment peut immédiatement retrouver ses données.

### Clean archi modulaire

Le concept de clean architecture est important à connaitre afin d'avoir une application robuste, avec des couches séparées et facilement testable.

Dans cette application, j'ai découpé les différentes couches en modules Android distincts afin de bien marquer cette séparation.

### organisation par feature

Au sein des modules j'ai essayé au maximum de regrouper les classes par feature plutôt que par type.

Un des avantages ici est que, comme l'application est relativement petite pour l'instant, cela limite le nombre de package différents.

L'autre avantage est que, même après ajout de nombreuses autres features, il sera facile de travailler sur la modification d'une feature en particulier car tous les fichiers seront dans les mêmes packages.

## Gradle CI

Pour la CI, j'ai appliqué des jobs assez simples compte-tenu de la taille de l'application, et du fait que je suis seul sur le projet.

On retrouver donc: 
- Lors des merge request, du linter et les tests , afin de s'assurer de la qualité du code avant de merger dans develop.

- Sur develop, on build l'application en debug, avec pour optique d'utiliser l'artefact généré pour une version alpha par exemple.

- Sur release et master, on build l'application en release afin de pouvoir facilement la publiée sur les store en prod.

Comme améliorations possibles dans le cas d'un vrai projet avec une équipe dédiée, on pourrait ajouter par exemple :

- une publication directement sur AppCenter pour les version de dev, alpha et beta (pour la publication sur le Playstore par contre, c'est peut-être mieux de faire une phase de recette avec des testeurs dédiés)

- l'exécution des tests d'intégration sur des machines virtuelles avec Docker

- l'exécution des tests UI sur une ferme de device (type SauceLabs)

- la creation de Tag git au moment de la creation de release

  

## Features (ajouter screenshots)

### Main

Pour l'activité principale, j'ai mis en place une bottom navigation qui permet d'accéder aux 4 fragments correspondants aux 4 features de l'app

  

### Home

Sur la home, la liste complète des chansons est affichées, avec possibilité de la rafraichir en faisant un pull-to-refresh.

Un clic sur une des chansons renvoi vers l'écran de lecture

Un clic sur l'icone de cœur d'une des chansons l'ajoute en tant que favoris.

![Accueil](/screenshots/home_fragment.png)

### Search

L'écran de recherche permet, comme son nom l'indique, d'effectuer une recherche de chanson par titre.

Comme sur la home, un clic sur une des chansons renvoi vers l'écran de lecture et un clic sur l'icone de cœur l'ajoute en tant que favoris.

![Recherche](/screenshots/search_fragment.png)

### Favorites

L'écran des favoris regroupe les chansons sauvegardées en tant que favoris dans l'application.

Un clic court sur l'un des favoris renvoi vers l'écran de lecture.

Un clic prolongé sur l'un des favoris déclenche le mode sélection multiple.

Depuis ce mode, plusieurs actions sont possibles :

- faire back pour revenir dans le mode de visualisation des favoris

- cliquer sur un favoris non-sélectionné pour l'ajouter à la sélection

- cliquer sur un favoris sélectionné pour le retirer de la sélection

- cliquer sur le FloatingActionButton pour supprimer les favoris sélectionnés

![Favoris](/screenshots/favorites_fragment.png)


### Player

Il s'agit d'un écran sans action réelle si ce n'est le fait d'alterner entre lecture et pause en cliquant sur le bouton central.

La pochette de la chansons correspond à la dernière chanson sélectionnée sur l'un des autres écrans.

Quand aucune chanson n'a été sélectionnée, le bouton de lecture n'est pas cliquable.

![Lecteur de musique](/screenshots/player_fragment.png)


### Remarques diverses

- l'application gère la rotation d'écran et préserve le scroll de la liste.
![Format paysage](/screenshots/landscape_mode.png "Light mode")


- l'application gère le dark mode comme on peut le voir sur les autres screenshots, mais uniquement depuis les paramètres systèmes d'Android.
![Light mode](/screenshots/light_mode.png "Light mode")


- l'application gère le mode offline, donc si pas d'accès à internet, les appels d'api ne déclenchent pas de messages d'erreur côté UI et renvoi à la place les données locales.

## Evolutions possibles

En imaginant que ceci était un vrai projet de production, on pourrait par la suite notamment continuer avec ces idées :

### features
- Gestion des albums en plus des chansons (mais en l'état, l'API ne fournie qu'une liste de chansons, en précisant uniquement l'id de l'album mais rien de plus)
- Faire en sorte que la notion de “favoris” ne soit qu’un paramètre des chansons stockées en base de donnée (dans le cas de l’exercice, j’ai fait autrement pour avoir une interaction entre différentes bases de données, mais je sais que ce choix n’est pas optimal dans le cas d’une vraie application)
- Ecran de paramétrage de l'application
- Lecture de "vraies" musiques dans le lecteur (issues de l'API ?)
- Mise en place d'un widget et d'une notification permanente pour gérer la musique sans lancer l'app
- Récupérer les musiques sur le téléphone pour compléter la liste de chansons
### Qualité du code
- Mise en place de TDD pour les prochaines features
- Faire plus de tests unitaires et d’intégration pour améliorer la couverture de test
- Activer l'obfuscation du code pour empêcher que l'on détourne le code de l'application
- Utiliser LeakCanary pour débusquer des fuites mémoire
- Utiliser l'inspecteur de code de Android Studio pour denicher des "code smells" par exemple
### Outilage
- Améliorer la CI comme expliqué plus haut
- Mise en place de la suite Firebase pour le suivi de prod (Crashlytics + Analytics)
